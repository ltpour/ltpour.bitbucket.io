(let ((project-root (file-name-directory load-file-name)))
  (setq org-publish-project-alist		
		`(("www"
		   :components ("www-pages" "www-static"))
		  ("www-pages"
		   :base-directory ,(concat project-root ".")
		   :base-extension "org"
		   :recursive t
		   :with-latex t
		   :publishing-directory ,(concat project-root  "..")
		   :publishing-function org-html-publish-to-html
		   :html-head "<link rel=\"stylesheet\" type=\"text/css\" href=\"static/sakura-earthly.css\"/>")
		  ("www-static"
		   :base-directory ,(concat project-root "./static")
		   :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|gz\\|tar\\|zip\\|bz2\\|xz\\|tex\\|txt\\|html\\|scm\\|key\\|svg"
		   :publishing-directory ,(concat project-root "../static")
		   :publishing-function org-publish-attachment
		   :recursive t))))
